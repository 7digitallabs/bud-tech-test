import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { of } from 'rxjs';
import Spy = jasmine.Spy;

import { AppComponent } from './app.component';
import { WorldBankApiService } from './services/world-bank-api/world-bank-api.service';
import { getNoMatchedCountriesResponse, getMatchedCountriesResponse, countriesStub } from './services/world-bank-api/world-bank-api.stub';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let component: AppComponent;
  let worldBankApiService: WorldBankApiService;
  let getCountryDetailsSpy: Spy;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        ReactiveFormsModule
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        WorldBankApiService
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    worldBankApiService = TestBed.inject(WorldBankApiService);
    getCountryDetailsSpy = spyOn(worldBankApiService, 'getCountryDetails');
    getCountryDetailsSpy.and.returnValue(of(getMatchedCountriesResponse));

    fixture.detectChanges();
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  describe('form validation', () => {
    it('should show a form error if the value entered is less than 2 characters', () => {
      component.countrySearch.setValue('s');
      component.countrySearch.updateValueAndValidity();
      fixture.detectChanges();

      expect(component.countrySearch.errors?.minlength).toBeTruthy();
      expect(component.countrySearch.errors?.minlength.requiredLength).toBe(2);
      expect(fixture.nativeElement.querySelector('mat-error').textContent.trim()).toEqual('Please enter between 2-3 characters');
    });

    it('should show a form error if the value entered is not a valid ISO code', () => {
      component.countrySearch.setValue('**');
      component.countrySearch.updateValueAndValidity();
      fixture.detectChanges();

      expect(component.countrySearch.errors?.minlength).toBeUndefined();
      expect(component.countrySearch.errors?.incorrectPattern).toBeTruthy();
      expect(fixture.nativeElement.querySelector('mat-error').textContent.trim()).toEqual('Please only enter an ISO code');
    });

    it('should show a form error if the value entered is not a valid ISO code', () => {
      component.countrySearch.setValue('123');
      component.countrySearch.updateValueAndValidity();
      fixture.detectChanges();

      expect(component.countrySearch.errors?.minlength).toBeUndefined();
      expect(component.countrySearch.errors?.incorrectPattern).toBeTruthy();
      expect(fixture.nativeElement.querySelector('mat-error').textContent.trim()).toEqual('Please only enter an ISO code');
    });
  });

  describe('searching for a country', () => {
    it('should not call the API if there is no search value entered', fakeAsync(() => {
      component.countrySearch.setValue('');
      component.countrySearch.updateValueAndValidity();
      tick(510);

      expect(worldBankApiService.getCountryDetails).not.toHaveBeenCalled();
    }));

    it('should not call the API if the form is not valid', fakeAsync(() => {
      component.countrySearch.setValue('123');
      component.countrySearch.updateValueAndValidity();
      tick(510);

      expect(component.countrySearch.valid).toBeFalse();
      expect(worldBankApiService.getCountryDetails).not.toHaveBeenCalled();
    }));

    it('should call the API if the form is valid and there is a value in the search field', fakeAsync(() => {
      component.countrySearch.setValue('br');
      component.countrySearch.updateValueAndValidity();
      tick(550);

      expect(component.countrySearch.valid).toBeTrue();
      expect(worldBankApiService.getCountryDetails).toHaveBeenCalledWith('br');
    }));

    it('should add properly debounce the changes to the search field', fakeAsync(() => {
      component.countrySearch.setValue('br');
      component.countrySearch.setValue('fr');
      component.countrySearch.setValue('de');
      tick(550);

      expect(worldBankApiService.getCountryDetails).toHaveBeenCalledOnceWith('de');
    }));

    it('should store the results metaData and array of countries', fakeAsync(() => {
      component.countrySearch.setValue('de');
      tick(550);

      expect((component as any)._resultMetaData).toEqual(getMatchedCountriesResponse[0]);
      expect(component.countries).toEqual(getMatchedCountriesResponse[1]);
    }));

    it('should store the response of the no results response in the _resultMetaData', fakeAsync(() => {
      getCountryDetailsSpy.and.returnValue(of(getNoMatchedCountriesResponse));
      component.countrySearch.setValue('de');
      tick(550);
      fixture.detectChanges();

      expect((component as any)._resultMetaData).toEqual(getNoMatchedCountriesResponse[0]);
    }));
  });

  it('should show an error message if there is no country match', fakeAsync(() => {
    getCountryDetailsSpy.and.returnValue(of(getNoMatchedCountriesResponse));
    component.countrySearch.setValue('de');
    tick(550);
    fixture.detectChanges();

    expect(fixture.nativeElement.querySelector('.no-items h2').textContent.trim()).toEqual('No country matches that code please enter a different 2/3 character ISO code.');
  }));

  describe('rendered country data', () => {
    let resultItems: NodeListOf<HTMLElement>;

    beforeEach(fakeAsync(() => {
      component.countrySearch.setValue('de');
      tick(550);
      fixture.detectChanges();

      resultItems = fixture.nativeElement.querySelectorAll('section .country');
    }));

    it('should show the country name for the matched country', () => {
      resultItems.forEach((block, index) => expect(block.querySelector('h1').textContent.trim()).toBe(countriesStub[index].name));
    });

    it('should show the country region if it exists', () => {
      resultItems.forEach((block, index) => {
        const contentCheck = Array.from(block.querySelectorAll('p'));
        const dataToShow = countriesStub[index].region.value;

        if (dataToShow) {
          expect(contentCheck.some(v => v.textContent === `Region: ${dataToShow}`)).toBeTrue();
        }
        else {
          expect(contentCheck.some(v => v.textContent === `Region: ${dataToShow}`)).toBeFalse();
        }
      });
    });

    it('should show the capitalCity if it exists', () => {
      resultItems.forEach((block, index) => {
        const contentCheck = Array.from(block.querySelectorAll('p'));
        const dataToShow = countriesStub[index].capitalCity;

        if (dataToShow) {
          expect(contentCheck.some(v => v.textContent === `Capital City: ${dataToShow}`)).toBeTrue();
        }
        else {
          expect(contentCheck.some(v => v.textContent === `Capital City: ${dataToShow}`)).toBeFalse();
        }
      });
    });

    it('should show the longitude if it exists', () => {
      resultItems.forEach((block, index) => {
        const contentCheck = Array.from(block.querySelectorAll('p'));
        const dataToShow = countriesStub[index].longitude;

        if (dataToShow) {
          expect(contentCheck.some(v => v.textContent === `Longitude: ${dataToShow}`)).toBeTrue();
        }
        else {
          expect(contentCheck.some(v => v.textContent === `Longitude: ${dataToShow}`)).toBeFalse();
        }
      });
    });

    it('should show the latitude if it exists', () => {
      resultItems.forEach((block, index) => {
        const contentCheck = Array.from(block.querySelectorAll('p'));
        const dataToShow = countriesStub[index].latitude;

        if (dataToShow) {
          expect(contentCheck.some(v => v.textContent === `Latitude: ${dataToShow}`)).toBeTrue();
        }
        else {
          expect(contentCheck.some(v => v.textContent === `Latitude: ${dataToShow}`)).toBeFalse();
        }
      });
    });
  });
});
