import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

import { NoCountriesResponse, RetrieveCountryDetailsResponse } from "./world-bank-api.interface";

@Injectable({
  providedIn: 'root'
})
export class WorldBankApiService {

  constructor(private _httpClient: HttpClient) { }

  public getCountryDetails(countryCode: string): Observable<RetrieveCountryDetailsResponse | NoCountriesResponse> {
    return this._httpClient.get<RetrieveCountryDetailsResponse>(`http://api.worldbank.org/v2/country/${countryCode}?format=json`);
  }
}
