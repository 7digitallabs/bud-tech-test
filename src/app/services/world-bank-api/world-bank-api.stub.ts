import { RetrieveCountryDetailsResponse, NoCountriesResponse, CountryDetails } from './world-bank-api.interface';

export const countriesStub: CountryDetails[] = [
  {
    id: 'FRA',
    iso2Code: 'FR',
    name: 'France',
    region: {
      id: 'ECS',
      iso2code: 'Z7',
      value: 'Europe & Central Asia'
    },
    adminregion: {
      id: '',
      iso2code: '',
      value: ''
    },
    incomeLevel: {
      id: 'HIC',
      iso2code: 'XD',
      value: 'High income'
    },
    lendingType: {
      id: 'LNX',
      iso2code: 'XX',
      value: 'Not classified'
    },
    capitalCity: 'Paris',
    longitude: '2.35097',
    latitude: '48.8566'
  },
  {
    id: 'FRA',
    iso2Code: 'FR',
    name: 'France',
    region: {
      id: '',
      iso2code: '',
      value: ''
    },
    adminregion: {
      id: '',
      iso2code: '',
      value: ''
    },
    incomeLevel: {
      id: '',
      iso2code: '',
      value: ''
    },
    lendingType: {
      id: '',
      iso2code: '',
      value: ''
    },
    capitalCity: '',
    longitude: '',
    latitude: ''
  }
];

export const getMatchedCountriesResponse: RetrieveCountryDetailsResponse = [
  {
    page: 1,
    pages: 1,
    per_page: '50',
    total: countriesStub.length
  },
  countriesStub
];

export const getNoMatchedCountriesResponse: NoCountriesResponse = [
  {
    message: [
      {
        id: '120',
        key: 'Invalid value',
        value: 'The provided parameter value is not valid'
      }
    ]
  }
]
