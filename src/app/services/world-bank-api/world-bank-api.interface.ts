export interface CountryNestedDetails {
  id: string;
  iso2code: string;
  value: string;
}

export interface SuccessResponseMetaData {
  pages: number;
  page: number;
  per_page: string;
  total: number;
}

export interface CountryDetails {
  id: string;
  iso2Code: string;
  name: string;
  region: CountryNestedDetails,
  adminregion: CountryNestedDetails,
  incomeLevel: CountryNestedDetails,
  lendingType: CountryNestedDetails,
  capitalCity: string;
  longitude: string;
  latitude: string;
}

export type RetrieveCountryDetailsResponse = [ SuccessResponseMetaData, CountryDetails[] ];

export interface NoCountryResponseItem {
  message: {
    id: string;
    key: string;
    value: string;
  }[]
}

export type NoCountriesResponse = [ NoCountryResponseItem ];
