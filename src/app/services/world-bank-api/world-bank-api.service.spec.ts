import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { WorldBankApiService } from './world-bank-api.service';
import { getMatchedCountriesResponse } from './world-bank-api.stub';

describe('WorldBankApiService', () => {
  let service: WorldBankApiService;
  let httpTestingController: HttpTestingController
  let matchedCountriesResponse = getMatchedCountriesResponse;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ]
    });

    service = TestBed.inject(WorldBankApiService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => httpTestingController.verify());

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('getCountryDetails()', () => {
    it('should make a GET request to the World Bank API', () => {
      const expectedEndpoint = `http://api.worldbank.org/v2/country/br?format=json`;

      service.getCountryDetails('br').subscribe(response => {
        expect(response).toEqual(matchedCountriesResponse);
      });

      const req = httpTestingController.expectOne(expectedEndpoint);

      expect(req.request.method).toBe('GET');
      req.flush(matchedCountriesResponse);
    });
  });
});
