import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormControl, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { of } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, switchMap, takeWhile } from 'rxjs/operators';

import { WorldBankApiService } from "./services/world-bank-api/world-bank-api.service";
import { CountryDetails, NoCountriesResponse, NoCountryResponseItem, RetrieveCountryDetailsResponse, SuccessResponseMetaData } from './services/world-bank-api/world-bank-api.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.scss' ]
})
export class AppComponent implements OnDestroy, OnInit {
  public countries: CountryDetails[];
  public countrySearch: FormControl;
  public loadingResults: boolean;
  public noResultsMessage: boolean;

  private _resultMetaData: SuccessResponseMetaData | NoCountryResponseItem | null;
  private _subscribed: boolean = true;

  constructor(private _worldBankApiService: WorldBankApiService) {
    this.countrySearch = new FormControl(null, [
      Validators.minLength(2),
      this.customValidator()
    ]);
  }

  /**
   * Validates that the search is between 2-3 characters long and has only characters a-z in it
   */
  customValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      if (!control.value?.trim()) {
        return null;
      }

      const twoOrThreeCharacters: RegExp = new RegExp(/^[a-z]{2,3}$/gi);
      return twoOrThreeCharacters.test(control.value?.trim()) ? null : { incorrectPattern: { value: control.value } };
    }
  }

  ngOnInit() {
    this.countrySearch.valueChanges
      .pipe(
        takeWhile(() => this._subscribed),
        debounceTime(500),
        distinctUntilChanged(),
        switchMap(value => {
          this.countries = [];
          this._resultMetaData = null;

          // Check if the form is valid and the search string has a value
          if (this.countrySearch.valid && this.countrySearch.value.trim()) {
            this.noResultsMessage = false;
            this.loadingResults = true;

            // Retrieve the country details from the World Bank
            return this._worldBankApiService.getCountryDetails(value).pipe(
              map((response: RetrieveCountryDetailsResponse | NoCountriesResponse) => {
                this._resultMetaData = response[0];
                this.countries = (response as RetrieveCountryDetailsResponse)[1];

                // Check if no results are returned as the country can't be found
                if (this._resultMetaData.hasOwnProperty('message')) {
                  this.noResultsMessage = true;
                }
              }),
            )
          }

          return of('results');
        })
      )
      .subscribe(() => {
        this.loadingResults = false;
      });
  }

  ngOnDestroy() {
    this._subscribed = false;
  }
}
